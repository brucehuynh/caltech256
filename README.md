# Caltech 256

Machine learning in Computer Vision UIT class code: CS332.J11
# Dependencies
----
scikit-learn
keras

# Result
----
#### Accuracy of model db2:
![Accuracy of db2](result/db2/result.png)


# Directories
----
```
bt_SVM
-db
--db1
---train.txt
---text.txt
--d2
---train.txt
---text.txt
-model
--svm_linear
---db1
----model.pkt
---db2
----model.pkt
-feature
--vgg16_fc2
---256 classes
-images
--256 classes
-test
--test image
-extract_features.py
-generate_db.py
-predict.py
-train.py
```
# Usage
----
## 1. Caltech dataset:
Click this [link](http://www.vision.caltech.edu/Image_Datasets/Caltech256/256_ObjectCategories.tar) to download caltech dataset, extract it and put into folder caltech256.
## 2. Generate training dataset:
```
python3 generate_db.py images db/db1
```
## 3. Feature extraction:
#### To extract feature of training:
```
python3 extract_features.py db/db1/train.txt
```
#### To extract feature of test:
```
python3 extract_features.py db/db1/test.txt
```
## 4. Training:
```
python3 train.py db/db1/train.txt db/db1/test.txt model/svm_linear/db1
```
## 5. Testing with your owm image:
```
python3 predict.py <<IMG_PATH> model/svmlinear/db1
```
Result will be printed on the terminal.



