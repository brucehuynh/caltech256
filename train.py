from sklearn import svm
from sklearn.externals import joblib
from sklearn import metrics
import numpy as np
import sys
import os
from PIL import ImageFile

def load_data(src):
    print("[+] Loading data........")
    data = []
    label = []
    with open(src, "r") as file:
        for i,line in enumerate(file):
            img_path = line[:-1]
            #print("[+] Read image  : ", img_path," id : ", i)
            if os.path.isfile(img_path) and img_path.find(".jpg") != -1:            
                save_path = img_path.replace("images", "features/vgg16_fc2").replace(".jpg", ".npy")
                label_temp = save_path.split("/")[2].split(".")[1]
                #print(label_temp)
                label.append(label_temp)
                #print(np.load(save_path)[0])
                data.append(np.load(save_path)[0])
    print("Data loaded!!")
    return (data,label)

def svm_linear(x_train,y_train,x_test,y_test):
    print("[+] SVM linear SVC....")
    clf = svm.SVC(kernel='linear').fit(x_train,y_train)
    predict = clf.predict(x_test)
    print("----------> Accuracy:",metrics.accuracy_score(y_test, predict))
    print("[+] Finished")
    return clf 

def savemodel(model,save_model_folder):
    if not os.path.exists(save_model_folder):
        os.makedirs(save_model_folder)
    os.chdir(save_model_folder)
    filename = "svm_linear.pkt"
    print("[+] Save model to : " ,save_model_folder,filename)
    joblib.dump(model,filename)

if __name__=="__main__":
    train = sys.argv[1]
    test = sys.argv[2]
    x_train, y_train = load_data(train)
    x_test, y_test = load_data(test)
    clf = svm_linear(x_train,y_train,x_test,y_test)
    save_model_folder = sys.argv[3]
    savemodel(clf,sys.argv[3])
    
