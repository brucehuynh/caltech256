from keras.preprocessing import image
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.applications.vgg16 import preprocess_input
import numpy as np
import sys
import os
from PIL import ImageFile
from sklearn import svm
from sklearn.externals import joblib
from sklearn import metrics

ImageFile.LOAD_TRUNCATED_IMAGES=True
print("[+] Setup model")
base_model = VGG16(weights='imagenet', include_top=True)
out = base_model.get_layer("fc2").output
model = Model(inputs=base_model.input, outputs=out)

def extract_features(path):
    img = image.load_img(path, target_size=(224, 224))
    img_data = image.img_to_array(img)
    img_data = np.expand_dims(img_data, axis=0)
    img_data = preprocess_input(img_data)
    print("[+] Extract feature from image : ", path)
    feature = model.predict(img_data)
    return feature

if __name__ =="__main__":
    img = sys.argv[1]
    model_path = sys.argv[2]
    feature = extract_features(img)
    clf = joblib.load(model_path)
    print("The Label of input image is: " + clf.predict(feature)[0])
    